{pkgs, lib, ...}:

let mod = "Mod4"; in 
{
	xsession.windowManager.i3 = {
		enable = true;
		package = pkgs.i3;
		config = {
			modifier = mod;
			gaps = {
				inner = 10;
				outer = 5;
			};

			keybindings = lib.mkOptionDefault {
				"${mod}+Return" = "exec wezterm";
				"${mod}+d" = "exec rofi -show drun";
			};
			bars = [];

		};
	};

	programs.rofi.enable=true;
	programs.feh.enable=true;
}
