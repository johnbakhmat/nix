{ config, pkgs, ...} :{
	imports = [
		./git.nix
		./i3.nix
		./modules/eww
	];

	home = {
		username="john";
		homeDirectory="/home/john";
		stateVersion = "24.05";
		packages = with pkgs; [];
	};

	programs.bash = {
		enable = true;
		shellAliases = {
			rebuild = "sudo nixos-rebuild switch";
		};
	};

	programs.neovim = {
		enable = true;
		defaultEditor = true;
	};
}
